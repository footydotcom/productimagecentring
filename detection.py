# General libs import
import io
import os
import sys
import uuid

import confuse
import logging
import datetime
import pathlib
import json
import requests
import pika
from pymongo import MongoClient
import gridfs
import boto3

# Neural network libs import
import tensorflow as tf
from object_detection.utils import label_map_util
from object_detection.builders import model_builder
from object_detection.utils import config_util

# Image processing libs import
import cv2
import numpy as np
from io import BytesIO
from PIL import Image

# MS Azure libs import
from azure.storage.blob import BlobServiceClient
from azure.servicebus import ServiceBusClient, ServiceBusMessage

target_height = 1280
target_width = 1280
SIZE = 456
CLASSIFICATION_THRESHOLD = 0.3
DETECTION_THRESHOLD = 0.5
items_list = ["Hill", "Other", "Toe"]

# Local paths
root_dir = os.getcwd()
CLASSIFICATION_MODEL_PATH = os.path.join(root_dir, "ProductTypeModel")
DIRECTION_MODEL_PATH = os.path.join(root_dir, "DirectionModel")
CHECKPOINT_PATH = os.path.join(root_dir, 'DetectionModel')

producttype_model = tf.keras.models.load_model(
    CLASSIFICATION_MODEL_PATH, compile=True)
direction_model = tf.keras.models.load_model(
    DIRECTION_MODEL_PATH, compile=True)

LABEL_MAP_NAME = os.path.join(root_dir, 'label_map.pbtxt')
PIPELINE_CONFIG = os.path.join(root_dir, 'pipeline.config')
config_file_path = os.path.join(root_dir, "config_default.yaml")

# Configuration file paths
config = confuse.Configuration('imageorientation', __name__)
config.set_file(config_file_path)

# Get Azure Service Bus settings from config.yaml
direction_list = config['computer_vision_labels']['image_direction'].get()
product_type_list = config['computer_vision_labels']['image_classification'].get(
)
product_type_shortlist = config['computer_vision_labels']['product_type_shortlist'].get(
)

# Load pipeline config and build a detection model
configs = config_util.get_configs_from_pipeline_file(PIPELINE_CONFIG)
detection_model = model_builder.build(
    model_config=configs['model'], is_training=False)

# Restore checkpoint for object detection model
ckpt = tf.compat.v2.train.Checkpoint(model=detection_model)
ckpt.restore(os.path.join(CHECKPOINT_PATH, 'ckpt-3')).expect_partial()
category_index = label_map_util.create_category_index_from_labelmap(
    LABEL_MAP_NAME)

# Create folder name and file name for log file in Blob storage
current_time = str(datetime.datetime.now(
    datetime.timezone.utc).strftime("%Y-%m-%dT%H:%M:%S.%f%Z"))
hour = current_time.split("T")[1][:2]
folder_name = current_time.split("T")[0]
log_file_name = (hour + ".log").replace(":", "")
directory_name = "PythonLogs"
sub_directory_name = "ImageOrientation"
blob_name = directory_name + "/" + sub_directory_name + \
    "/" + folder_name + "/" + log_file_name

# Set up file and smtp loggers
logger_tofile = logging.getLogger(__name__)
logger_tofile.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s:%(name)s:%(message)s')

file_handler = logging.FileHandler(log_file_name)
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(formatter)

logger_tofile.addHandler(file_handler)

db = MongoClient(os.environ['mongo_connection_string'])
fs_write = gridfs.GridFS(db[os.environ['app_database']], "CenteredImages")
fs_read = gridfs.GridFS(db['ImageCache'], "Images")
s3 = boto3.resource(service_name='s3', region_name='eu-west-1', aws_access_key_id=os.environ['aws_access_key_id'], aws_secret_access_key=os.environ['aws_secret_access_key'])


def retrieve_image_from_url(image_url):
    try:
        bucket = s3.Bucket(os.environ['aws_bucket_name'])
        image_obj = bucket.Object(image_url)
        img_data = image_obj.get().get('Body').read()
        image = Image.open(BytesIO(img_data))
        image_np = np.array(image)
        image_height, image_width = np.shape(image_np)[:2]
        img_tensor = tf.convert_to_tensor(image_np)
        img_tensor = tf.image.resize(img_tensor, (SIZE, SIZE))
        img_tensor = tf.expand_dims(img_tensor, 0)
    except Exception as e:
        logger_tofile.error('Image URLS is corrupted', exc_info=sys.exc_info())
        return None
    return img_tensor, image_np, image_width, image_height


def product_type_prefiltering(tensor):
    classes = producttype_model.predict(tensor)
    probability = np.amax(classes)
    index = np.argmax(classes)
    text_class = product_type_list[index]
    if text_class in product_type_shortlist and probability > CLASSIFICATION_THRESHOLD:
        true_class = True
    else:
        true_class = False
    return true_class, text_class


def direction_inference(tensor, image_np):
    classes = direction_model.predict(tensor)
    index = np.argmax(classes)
    text_class = direction_list[index]
    if text_class == "Toe":
        flipped_image_np = cv2.flip(image_np, 1)
        return flipped_image_np
    elif text_class == "Hill":
        return image_np
    else:
        return None


def logs_to_blob():
    logging_storage_connections_str = os.environ['logging_storage_connections_str']
    logging_container_name = os.environ['logging_container_name']
    # Create the BlobServiceClient object which will be used to create a container client
    blob_service_client = BlobServiceClient.from_connection_string(
        logging_storage_connections_str)
    upload_file_path = log_file_name
    # Create a blob client using the local file name as the name for the blob
    blob_client = blob_service_client.get_blob_client(
        container=logging_container_name, blob=blob_name)
    # Upload the created file
    with open(upload_file_path, "rb") as data:
        blob_client.upload_blob(data, overwrite=True)
    logging.shutdown()
    pathlib.Path(upload_file_path).unlink()


def detect_fn(image):
    image, shapes = detection_model.preprocess(image)
    prediction_dict = detection_model.predict(image, shapes)
    detections = detection_model.postprocess(prediction_dict, shapes)
    return detections


def predict_bboxes(image_np):
    tensor = tf.convert_to_tensor(
        np.expand_dims(image_np, 0), dtype=tf.float32)
    detections = detect_fn(tensor)

    num_detections = int(detections.pop('num_detections'))
    detections = {key: value[0, :num_detections].numpy()
                  for key, value in detections.items()}
    detections['num_detections'] = num_detections

    # detection_classes should be ints.
    detections['detection_classes'] = detections['detection_classes'].astype(
        np.int64)

    # This is the way I'm getting my coordinates
    boxes = detections['detection_boxes']
    # get all boxes from an array
    max_boxes_to_draw = boxes.shape[0]
    # get scores to get a threshold
    scores = detections['detection_scores']
    # this is set as a default but feel free to adjust it to your needs
    min_score_thresh = DETECTION_THRESHOLD
    # iterate over all objects found
    for i in range(min(max_boxes_to_draw, boxes.shape[0])):
        #
        if scores is None or scores[i] > min_score_thresh:
            return boxes[i].tolist()
        else:
            return None


def background_layout_creation(image_np, image_height, image_width):
    img_gray = cv2.cvtColor(image_np, cv2.COLOR_RGB2GRAY)
    bottom_left_coner = img_gray[0, 0]
    bottom_right_coner = img_gray[0, image_width - 1]
    top_left_coner = img_gray[image_height - 1, 0]
    top_right_coner = img_gray[image_height - 1, image_width - 1]
    # Create a color range
    top_color = img_gray[0, 0] + 10
    bottom_color = img_gray[0, 0] - 10
    # Check if all corners if they are whithin a color range
    if top_color >= bottom_left_coner >= bottom_color and \
            top_color >= bottom_right_coner >= bottom_color and \
            top_color >= top_left_coner >= bottom_color and \
            top_color >= top_right_coner >= bottom_color:
        return True
    else:
        print("Different background colors")
        return None


def accurate_object_localization(image_np, bboxes, image_height, image_width):
    img_gray = cv2.cvtColor(image_np, cv2.COLOR_BGR2GRAY)
    background_color = img_gray[1, 1]
    height, width = [image_height - 1, image_width - 1]
    y_min, x_min, y_max, x_max = bboxes
    x_min = int(x_min * width)
    x_max = int(x_max * width)
    y_max = int(y_max * height)
    y_min = int(y_min * height)
    top_color = background_color + 10
    bottom_color = background_color - 10
    # 1 Find x_min
    x_min_up = 0
    y = y_min
    while y < y_max:
        y += 1
        gray_value = img_gray[y, x_min]
        if gray_value >= top_color or gray_value <= bottom_color:
            x_min_up += 1
            if x_min_up > 0:
                break
    if x_min_up > 0:  # |<-- x_min
        y = y_min
        while y < y_max:
            y += 1
            gray_value = img_gray[y, x_min]
            if gray_value >= top_color or gray_value <= bottom_color:
                y = y_min
                if x_min > 0:
                    x_min -= 1
                else:
                    break
    else:  # x_min -->|
        stop = 0
        while stop == 0:
            y = y_min
            x_min += 1
            while y < y_max:
                y += 1
                gray_value = img_gray[y, x_min]
                if top_color >= gray_value >= bottom_color:
                    continue
                else:
                    stop += 1
                    break
    # 2 Find x_max
    y = y_min
    x_max_up = 0
    while y < y_max:
        y += 1
        gray_value = img_gray[y, x_max]
        if gray_value >= top_color or gray_value <= bottom_color:
            x_max_up += 1
            if x_max_up > 0:
                break
    if x_max_up > 0:  # | x_max -->|
        y = y_min
        while y < y_max:
            y += 1
            gray_value = img_gray[y, x_max]
            if gray_value >= top_color or gray_value <= bottom_color:
                y = y_min
                if x_max < width:
                    x_max += 1
                else:
                    break
    else:  # |<-- x_max
        stop = 0
        while stop == 0:
            y = y_min
            x_max -= 1
            while y < y_max:
                y += 1
                gray_value = img_gray[y, x_max]
                if top_color >= gray_value >= bottom_color:
                    continue
                else:
                    stop += 1
                    break
    # 3 Find y_min
    x = x_min
    y_min_up = 0
    while x < x_max:
        x += 1
        gray_value = img_gray[y_min, x]
        if gray_value >= top_color or gray_value <= bottom_color:
            y_min_up += 1
            if y_min_up > 0:
                break
    if y_min_up > 0:  # y_min --> up
        x = x_min
        while x < x_max:
            x += 1
            gray_value = img_gray[y_min, x]
            if gray_value >= top_color or gray_value <= bottom_color:
                x = x_min
                if y_min > 0:
                    y_min -= 1
                else:
                    break
    else:  # y_min --> down
        stop = 0
        while stop == 0:
            x = x_min
            y_min += 1
            while x < x_max:
                x += 1
                gray_value = img_gray[y_min, x]
                if top_color >= gray_value >= bottom_color:
                    continue
                else:
                    stop += 1
                    break
    # 4 Find y_max
    x = x_min
    y_max_up = 0
    while x < x_max:
        x += 1
        gray_value = img_gray[y_max, x]
        if gray_value >= top_color or gray_value <= bottom_color:
            y_max_up += 1
            if y_max_up > 0:
                break
    if y_max_up > 0:  # y_max --> down
        x = x_min
        while x < x_max:
            x += 1
            gray_value = img_gray[y_max, x]
            if gray_value >= top_color or gray_value <= bottom_color:
                x = x_min
                if y_max < height:
                    y_max += 1
                else:
                    break
    else:  # y_max --> up
        stop = 0
        while stop == 0:
            x = x_min
            y_max -= 1
            while x < x_max:
                x += 1
                gray_value = img_gray[y_max, x]
                if top_color >= gray_value >= bottom_color:
                    continue
                else:
                    stop += 1
                    break
    return y_min, x_min, y_max, x_max


def object_cropping(image_np, y_min, x_min, y_max, x_max, product_type):
    cropped_img = image_np[y_min:y_max, x_min:x_max]
    target_color = Image.fromarray(cropped_img).convert("RGB").getpixel((0, 0))
    background_layout = Image.new(
        "RGB", (target_height, target_width,), target_color)
    if product_type == "Boots":
        indent = 0.95
    else:
        indent = 1.0
    crop_height, crop_width = np.shape(cropped_img)[:2]
    resize_coeff = target_height / max(crop_width, crop_height)
    width_crop = int(resize_coeff * crop_width * indent)
    height_crop = int(resize_coeff * crop_height * indent)
    dsize = (width_crop, height_crop)
    cropped_img = Image.fromarray(cv2.resize(cropped_img, dsize=dsize))
    offset = ((target_width - width_crop) // 2,
              (target_height - height_crop) // 2)
    background_layout.paste(cropped_img, offset)
    img_byte_arr = io.BytesIO()
    background_layout.save(img_byte_arr, format="webp")
    return img_byte_arr


def save_to_db(image_bytes, image_url):
    try:
        query = fs_write.find({'filename': image_url}).limit(
            1)  # equivalent to find_one
        # Iterate GridOutCursor, should have either one element or None
        content = next(query, None)
        if content:
            fs_write.delete(content._id)
    finally:
        fs_write.put(image_bytes.getvalue(), filename=image_url)


def predict(ch, method, properties, body):
    report_message = {"success": None, "image_url": None}
    try:
        message = json.loads(str(body, "utf-8"))
        image_url = message["image_url"]
        img_tensor, image_np, image_width, image_height = retrieve_image_from_url(
            image_url)
        if image_np is not None:
            whitelist, product_type = product_type_prefiltering(img_tensor)
            if whitelist:
                direction_image = direction_inference(img_tensor, image_np)
                if direction_image is not None:
                    bboxes = predict_bboxes(direction_image)
                    if bboxes is not None:
                        background_layout = background_layout_creation(direction_image, image_height,
                                                                       image_width)
                        if background_layout:
                            y_min, x_min, y_max, x_max = accurate_object_localization(direction_image,
                                                                                      bboxes,
                                                                                      image_height,
                                                                                      image_width)
                            image_bytes = object_cropping(
                                direction_image, y_min, x_min, y_max, x_max, product_type)
                            save_to_db(image_bytes, image_url)
                            report_message["success"] = True
                            report_message["image_url"] = image_url
                            logger_tofile.info("Success")
                        else:
                            img_byte_arr = io.BytesIO()
                            Image.fromarray(direction_image).save(
                                img_byte_arr, format="webp")
                            save_to_db(img_byte_arr, image_url)
                            report_message["success"] = True
                            report_message["message"] = "Flip of original image only"
                            report_message["image_url"] = image_url
                            logger_tofile.info("Success")
                    else:
                        logger_tofile.info("Shoes wasn't detected")
                        report_message["success"] = False
                        report_message["message"] = "Shoes wasn't detected"
                else:
                    logger_tofile.info("It's not Toe-Hill image")
                    report_message["success"] = False
                    report_message["message"] = "It's not Toe-Hill image"
            else:
                logger_tofile.info("Product Type is OOD")
                report_message["success"] = False
                report_message["message"] = "Product Type is OOD"
        else:
            logger_tofile.info("Image URL is corrupted")
            report_message["success"] = False
            report_message["message"] = "Image URL is corrupted"
        report_message = json.dumps(report_message)
    except Exception as e:
        logger_tofile.error('Message format is incompatible',
                            exc_info=sys.exc_info())
        report_message["success"] = False
        report_message["message"] = "Message format is incompatible"
        report_message = json.dumps(report_message)
    finally:
        print(report_message)
        ch.basic_ack(delivery_tag=method.delivery_tag)


def subscribe_to_queue():
    credentials = pika.PlainCredentials(
        os.environ['rabbit_username'], os.environ['rabbit_password'])
    parameters = pika.ConnectionParameters(os.environ['rabbit_host'],
                                           5672,
                                           os.environ['env'],
                                           credentials)

    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.basic_qos(prefetch_count=1)
    result = channel.queue_declare(
        queue='CenterImage', exclusive=False, durable=True, auto_delete=False)
    queue_name = result.method.queue

    print(' [*] Waiting for Messages. To exit press CTRL+C')

    channel.basic_consume(
        queue=queue_name, on_message_callback=predict, auto_ack=False)

    channel.start_consuming()


if __name__ == '__main__':
    subscribe_to_queue()
