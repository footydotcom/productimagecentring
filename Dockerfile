FROM python:3.8 AS base

FROM base AS build
# install build utilities
RUN apt-get update && \
    apt-get install -y --no-install-recommends apt-utils && \
    apt-get install -y libgl1-mesa-glx && \
    apt-get -y upgrade

# clone the repository 
RUN git clone --depth 1 https://github.com/tensorflow/models.git

# Install object detection api dependencies
RUN apt-get install -y python3-pip protobuf-compiler python-tk && \
    pip install lxml && \
    pip install Cython && \
    pip install contextlib2 && \
    pip install jupyter && \
    pip install matplotlib && \
    pip install pycocotools && \
    pip install opencv-python && \
    pip install flask && \
    pip install tensorflow-cpu && \
    pip install Pillow && \
    pip install tf_slim && \
    pip install tensorflow-io && \
    pip install requests && \
    pip install scipy && \
    pip install tf-models-official && \
    pip install confuse && \
    pip install wget && \
    pip install azure-servicebus && \
    pip install azure-storage-blob && \
    pip install pika && \
    pip install pymongo && \
    pip install boto3


# Get protoc 3.0.0, rather than the old version already in the container
RUN curl -OL "https://github.com/google/protobuf/releases/download/v3.0.0/protoc-3.0.0-linux-x86_64.zip" && \
    unzip protoc-3.0.0-linux-x86_64.zip -d proto3 && \
    mv proto3/bin/* /usr/local/bin && \
    mv proto3/include/* /usr/local/include && \
    rm -rf proto3 protoc-3.0.0-linux-x86_64.zip

# Run protoc on the object detection repo
RUN cd models/research && \
    protoc object_detection/protos/*.proto --python_out=.

# Set the PYTHONPATH to finish installing the API
ENV PYTHONPATH=$PYTHONPATH:/models/research/object_detection
ENV PYTHONPATH=$PYTHONPATH:/models/research/slim
ENV PYTHONPATH=$PYTHONPATH:/models/research

# set this as the working directory

COPY /DetectionModel /productimagecentring/DetectionModel
COPY /DirectionModel /productimagecentring/DirectionModel
COPY /ProductTypeModel /productimagecentring/ProductTypeModel


FROM build AS detection

COPY *.* /productimagecentring/
WORKDIR /productimagecentring

FROM detection AS publish
CMD ["python", "detection.py"]
